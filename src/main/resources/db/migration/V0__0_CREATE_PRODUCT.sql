CREATE TABLE product
(
    id          BIGINT         NOT NULL AUTO_INCREMENT,
    sku         VARCHAR(100)   NOT NULL,
    name        VARCHAR(255)   NOT NULL,
    description TEXT,
    price       DECIMAL(10, 2) NOT NULL,
    PRIMARY KEY (id)
);
